class Person
  attr_accessor :age
  def initialize(initial_age)
    if initial_age <0
      initial_age = 0
      puts 'Age is not valid, setting age to 0.'
    end
    self.age=initial_age
  end

  def amIOld()
    case
      when @age < 13
        puts 'You are young.'
      when @age >= 18
        puts 'You are old.'
      else
        puts 'You are a teenager.'
    end
  end

  def yearPasses()
    self.age += 1
  end
end