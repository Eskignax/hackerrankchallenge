# Enter your code here. Read input from STDIN. Print output to STDOUT
def factorial(num)
  if num == 0
    result = 1
  else
    result = num * factorial(num-1)
  end
  result
end

puts factorial(STDIN.gets.to_i)