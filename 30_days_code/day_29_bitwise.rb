#t = gets.strip.to_i
#tc = [1]
#for a0 in (0..t-1)
#  n,k = gets.strip.split(' ')
#  tc << [[n.to_i, k.to_i]]
#end

#Solution 1 too long
def solution_one
  tc = [[14, 2], [14, 3], [14, 4], [14, 5], [14,6], [14,7], [14,8], [14,9], [14,10], [14,11], [14, 12], [14, 13], [14,14]]
  t = tc.length
  max = tc.max_by { |v| v[0] }[0]
  s = [*1..max]
  datas = {}
  (s.length - 1).times do
    (s.length - 1).times do |i|
      i+=1
      datas[[s[0], s[i]]] = s[0]&s[i]
    end
    s.shift
  end

  t.times do |i|
    n = tc[i][0]
    max_k = tc[i][1]
    puts datas.select { |k, v| k[0] <= n && k[1] <= n && v < max_k }.values.max
  end
end

#Solution 2 on the fly
def solution_two
  t=3
  tc = [[5, 2], [8, 5], [2, 2]]
  max = tc.max_by { |v| v[0] }[0]
  s = [*1..max]
  datas = {}
  tc_result = Array.new(t, 0)
  tc_current = Array.new(t, 0)
  (s.length - 1).times do
    (s.length - 1).times do |i|
      i+=1
      datas[[s[0], s[i]]] = s[0]&s[i]
      tc_current.length.times do |m|
        tc_current[m] = datas[[s[0], s[i]]] if datas[[s[0], s[i]]] < tc[m][1] && datas[[s[0], s[i]]] > tc_current[m] && s[i] <= tc[m][0]
      end
    end
    s.shift
  end
  puts tc_current
end


def solution_three
  t=3
  tcs = []
#  for a0 in (0..t-1)
#    n,k = gets.strip.split(' ')
#    tcs << TestCase.new(n.to_i, k.to_i, a0)
#  end

  tcs << TestCase.new(5, 2, 0)
  tcs << TestCase.new(8, 5, 1)
  tcs << TestCase.new(2, 2, 2)

  max = tcs.max_by { |v| v.n_max }.n_max
  max = 999
  s = [*1..max]
  tcs_current = Hash[tcs.map.with_index { |x, i| [i, x] }]
  (s.length - 1).times do
    (s.length - 1).times do |i|
      i+=1
      bitwise = s[0]&s[i]
      tcs_current.values.each do |tc|
        if s[0] >= tc.n_max
          tcs[tc.index] = tc
          tcs_current.delete(tc.index)
          elsif bitwise < tc.k_less && bitwise > tc.result && s[i] <= tc.n_max
          tc.result = bitwise
          if tc.result == tc.k_less - 1
            tcs[tc.index] = tc
            tcs_current.delete(tc.index)
          end
        end
      end
    end
    s.shift
  end
  puts tcs.collect{ |tc| tc.result}
end

class TestCase
  attr_accessor :n_max, :k_less, :found, :result, :index

  def initialize(n, k, index)
    @result = 0
    @n_max = n
    @k_less = k
    @found = false
    @index = index
  end

end

#the stupid_way
def solution_four
  t = gets.strip.to_i
#  t = 1
  for a0 in (0..t-1)
    n,k = gets.strip.split(' ')
    n = n.to_i
    k = k.to_i
   # n = 2
   # k = 2
    s = [*1..n]
    result = 0
    (s.length - 1).times do
      (s.length - 1).times do |i|
        i = i+1
        bitwise = s[0]&s[i]
        result = bitwise if bitwise < k && bitwise > result
      end
      s.shift
    end
    puts result
  end

end

#fcking math
def solution_five
  t = gets.strip.to_i
  for a0 in (0..t-1)
    n,k = gets.strip.split(' ')
    n = n.to_i
    k = k.to_i
    if (k-1|k) <= n
      puts k-1
    else
      puts k-2
    end
  end
end

#def tester solution
def find_max(n,k)
  max = 0
  a = n - 1
  while a > 0 do
    for b in ((a+1)..n) do
      test = a & b
      if test < k && test > max
        max = test
      end
    end
    a -= 1
  end
  max
end

def tester_solution
  t = gets.strip.to_i
  for a0 in (0..t-1)
    n,k = gets.strip.split(' ')
    n = n.to_i
    k = k.to_i
    puts find_max(n,k)
  end
end



