n=6


case
  when n.odd?
    puts 'Weird'
  when n.between?(2, 5)
    puts 'Not Weird'
  when n.between?(6, 20)
    puts 'Weird'
  else
    puts 'Not Weird'
end