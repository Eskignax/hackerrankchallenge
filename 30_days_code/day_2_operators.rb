# Enter your code here. Read input from STDIN. Print output to STDOUT
def percent(p,total)
  total*(p/100)
end

meal_cost = STDIN.gets.to_f
tip_percent = STDIN.gets.to_f
tax_percent = STDIN.gets.to_f

tip=percent(tip_percent,meal_cost)
tax=percent(tax_percent,meal_cost)

total_cost=meal_cost + tip + tax
puts "The total meal cost is #{total_cost.round} dollars."