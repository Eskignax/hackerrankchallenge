#actual = gets.split
actual = [31, 12, 2009]
da = actual[0].to_i
ma = actual[1].to_i
ya = actual[2].to_i

#expected = gets.split
expected = [1, 1, 2010]
de = expected[0].to_i
me = expected[1].to_i
ye = expected[2].to_i

fee = 0

if ya > ye
  #late for year
  fee = 10000
elsif ma > me && ya == ye
  #late for month
  fee = 500 * (ma - me)
elsif da > de && ma == me
  #late for day
  fee = 15 * (da - de)
end

puts fee