class Person
  def initialize(firstName, lastName, id)
    @firstName = firstName
    @lastName = lastName
    @id = id
  end
  def printPerson()
    print("Name: ",@lastName , ", " + @firstName ,"\nID: " , @id)
  end
end

class Student <Person
  attr_accessor :scores

  def initialize(firstName, lastName, id, scores)
    super(firstName,lastName,id)
    @scores = scores
  end

  def calculate
    average = @scores.reduce(:+) / @scores.size
    case average
      when 90..100
        'O'
      when 80...90
        'E'
      when 70...80
        'A'
      when 55...70
        'P'
      when 40...55
        'D'
      else
        'T'
    end
  end

end

input = ["Alexis","MyLastName","1234567"]
firstName = input[0]
lastName = input[1]
id = input[2].to_i
scores = [100,80]
s = Student.new(firstName, lastName, id, scores)
s.printPerson
print("\nGrade: " + s.calculate)