class Node
  attr_accessor :head, :value, :left, :player, :childs

  def initialize(head, value)
    self.head = head
    self.value = value
    if head.player == :P1
      self.player = :P2
    else
      self.player = :P1
    end
    self.player = player
    self.left = head.left - value
    self.childs = []
    [2, 3, 5].each do |v|
      self.childs << Node.new(self, v) if self.left >= v
    end
  end
end

class Head < Node
  def initialize(left, player)
    self.head = nil
    self.value = 0
    self.player = player
    self.left = left
    self.childs = []
    [2, 3, 5].each do |v|
      self.childs << Node.new(self, v) if self.left >= v
    end
  end
end

head_node = Head.new(max_value, :P1)