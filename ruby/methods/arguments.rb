#In this challenge, your task is to figure out what take method
#does using the examples below and implement the method.
#It should help you understand how to build on implementation through
#the expected functionality.
def take(numbers, index=1 )
  numbers.shift(index)
  numbers
end