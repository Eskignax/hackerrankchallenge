def rot13(secret_messages)
  lowercase_array = [*('a'..'z')]
  uppercase_array = [*('A'..'Z')]
  # your code here
  decoded_message = []
  secret_messages.map do |words|
    decoded_word = ""
    words.each_char  do |c|
      case c
        when /[[:upper:]]/
          decoded_word += uppercase_array[(uppercase_array.find_index(c) + 13) % 26]
        when /[[:lower:]]/
          decoded_word += lowercase_array[(lowercase_array.find_index(c) + 13) % 26]
        else
          decoded_word += c
      end
    end
    decoded_message << decoded_word
  end
  decoded_message
end

rot13(["Gb trg gb gur bgure fvqr!"])