# Enter your code here. Read input from STDIN. Print output to STDOUT
x = 100
n = 2

def pow(value, times)
  if times > 1
    value * pow(value, (times-1))
  else
    value
  end
end

def check(head, array_value, sol, x)
  array_value.size.times do
    new_head = head + array_value.shift
    sol << new_head
    check(new_head, array_value.dup, sol, x) if new_head < x
  end
end

array_value = []
sol = []
x.times do |i|
  pow_value = pow(i+1,n)
  break if pow_value > x
  array_value[i] = pow_value
end

check(0,array_value, sol, x)

puts sol.select{|v| v == x}.size