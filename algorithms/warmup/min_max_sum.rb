#!/bin/ruby

array = gets.strip.split(' ').map(&:to_i)
array.sort!
min = array[0,4].reduce(:+)
max = array[1,4].reduce(:+)
puts "#{min} #{max}"
