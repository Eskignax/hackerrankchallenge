# Enter your code here. Read input from STDIN. Print output to STDOUT

def solution(a, b, n)
  result = 0
  (n-2).times do |i|
    result = a + (b**2)
    a = b
    b = result
  end
  puts result
end

first = 0
second = 1
n = 5
solution(first, second, n)
