def candies(kids_rating)
kids_rating ||= [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
nb_of_kids = kids_rating.length
if nb_of_kids <= 1
  puts nb_of_kids
else
  candies_sum=0
  previous_kid_candie=0
  previous_rate=0
  direction=:same
  suite=0
  previous_up_candies=0
  kids_rating.each do |rate|
    if rate > previous_rate #DIRECTION UP
      previous_kid_candie +=1
      candies_sum += previous_kid_candie
      if direction == :up
        suite+=1
      else
        suite=1
        direction = :up
      end
    elsif rate < previous_rate #DIRECTION DOWN
      if direction == :down
        suite+=1
        if suite == previous_up_candies
          suite += 1 #we include the first kids of the suite in the list
        end
        candies_sum+=suite
      else
        previous_up_candies=previous_kid_candie
        previous_kid_candie = 1
        suite=1
        candies_sum += 1
        if previous_up_candies == previous_kid_candie
          #we include in the suite
          candies_sum += 1
          suite+=1
        end
      end
      direction = :down
    else # DIRECTION SAME
      previous_kid_candie = 1
      candies_sum +=1
      direction = :same
    end
    previous_rate = rate
  end
  candies_sum
end
end


#testcase
#uponly
puts "#{10 == candies([3, 4 ,5, 6])}"
#downonly
puts "#{55 == candies([10, 9, 8, 7, 6, 5, 4, 3, 2, 1])}"
#sameonly
puts "#{5 == candies([1, 1, 1, 1, 1])}"
#up_and_down equilibrate
puts "#{9 == candies([1, 2, 3, 2, 1])}"
#up_and_down left
puts "#{13 == candies([1, 2, 3, 4, 2, 1])}"
#up_and_down right
puts "#{13 ==candies([1, 2, 4, 3, 2, 1])}"