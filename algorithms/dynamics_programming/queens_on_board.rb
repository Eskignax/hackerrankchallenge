class Board
  attr_accessor :lines, :columns, :grid

  def initialize(lines, columns)
    @lines=lines
    @columns=columns
  end

  def fill_grid
    @grid=[]
    lines.times do
      @grid << STDIN.gets.split(" ")
    end
  end
end

class TestCase
  attr_accessor :board, :keys, :value

  #method to cache the results
  def hash_board
  end

end

class Game
  attr_accessor :test_cases
end


number_of_test_case = STDIN.gets.to_i
number_of_test_case.times do
  n,m = STDIN.gets.split(" ")
  board = Board.new(n.to_i, m.to_i)
  board.fill_grid
  puts board.grid
end