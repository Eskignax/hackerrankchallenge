class DataDynamic
  attr_accessor :dir, :values

  def initialize(dir, index1, value1, index2, value2)
    @values={index1 => value1, index2 => value2}
    @dir=dir
  end

  def to_s
    "#{@dir}:#{@values}"
  end
end

def direction(arr, i)
  case
    when arr[i] > arr[i+1]
      direction = :down
    when arr[i] < arr[i+1]
      direction = :up
    else
      direction = :equal
  end
  direction
end


def candies
  nb_of_kids = 10 #STDIN.gets.to_i
  kids_rating = []
  kids_rating = [2,4,2,6,1,7,8,9,2,1]
  kids_candies = Array.new(nb_of_kids, 0)
 # nb_of_kids.times do
 #   kids_rating << STDIN.gets.to_i
 #  end
  if nb_of_kids <= 1
    puts nb_of_kids
  else
    arr_datas=[]
    current_d=direction(kids_rating, 0)
    arr_datas[0]=DataDynamic.new(current_d, 0, kids_rating[0], 1, kids_rating[1])
    point_of_start = 0
    i=1
    while i<nb_of_kids-1
      i_d = direction(kids_rating, i)
      if i_d == current_d
        arr_datas[point_of_start].values[i+1] = [kids_rating[i+1], 0]
      else
        point_of_start+=1
        arr_datas[point_of_start]=DataDynamic.new(i_d, i, kids_rating[i], i+1, kids_rating[i+1])
        current_d = i_d
      end
      i+=1
    end
    arr_datas.length.times do
      max_length = arr_datas.max_by do |element|
        element.values.length
      end
      max_length.values.each_with_index do |v, i|
        case max_length.dir
          when :down
            kids_candies[v[0]] = max_length.values.length - (i) if kids_candies[v[0]] == 0
          when :up
            kids_candies[v[0]] = i+1 if kids_candies[v[0]] == 0
          else
            kids_candies[v[0]] = 1 if kids_candies[v[0]] == 0
        end
      end
      arr_datas.delete(max_length)
    end
    puts kids_candies.inject(0) { |sum, x| sum + x }
  end
end

candies