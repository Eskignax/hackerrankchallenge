def sherlock(dig)
  mod3 = dig%3
  div3 = dig/3
  mod5 = dig%5
  div5 = dig/5
  if div3 <= 0 && div5 <= 0
    puts '-1'
  elsif mod3 == 0
    puts "#{'555'*div3}"
    else
    nb_of_3 = 0
    save_rest = 0
    div3.step(1, -1) do |i|
      rest = dig-(3*(i))
      if rest%5 == 0
        nb_of_3 = i
        save_rest = rest
        break
      end
    end
    nb_of_5 = 0
    unless save_rest== 0
      nb_of_5=save_rest/5
    end
    if nb_of_3 == 0 && save_rest == 0
      puts '-1'
    else
    puts "#{'555'*nb_of_3}#{'33333'*nb_of_5}"
      end
  end

end

sherlock 4
sherlock 7

