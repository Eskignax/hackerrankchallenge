
def game(input)
  t = input.shift.to_i
  input = input.map(&:to_i)
  max_value = input.max
  result = Array.new(max_value+1)
  result[0] = :looser
  result[1] = :looser
  (2..max_value).each do |n|
    if result[n-2] == :looser
      result[n] = :winner
    elsif n >= 3 &&result[n-3] == :looser
      result[n] = :winner
    elsif n >= 5 && result[n-5] == :looser
      result[n] = :winner
    else
      result[n] = :looser
    end
  end
  input.each do |n|
    if result[n] == :winner
      puts "First"
    else
      puts "Second"
    end
  end
end

  game("1\n7".split(/\n+/))

